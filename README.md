# ESP8266 Simple SONOS Controller

A controller for KAL, that plays pre-selected content, based on a
button press.

The device uses deep sleep, to conserve power.

# Notes

- Uses Wemos D1 mini
- 3 AA batteries, wired directly to the 5V pin
- Watches Vcc line for power drop on boot (aka, check for not 3V)
- 6 buttons, charlieplexed
- 2 LEDs for indicators (green and red)
- payload is simple SOAP UPnP, wiresharked
