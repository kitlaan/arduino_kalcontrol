#include <Arduino.h>

#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>

#include <ArduinoOTA.h>

#include "config.h"
#include "upnp.h"

#define VOLTAGE_FROM_VCC
#ifdef VOLTAGE_FROM_VCC
    ADC_MODE(ADC_VCC);
    #define MIN_VOLTAGE 3010
#endif

#define SLEEP_SECONDS 0
#define DISPLAY_A_DIM 90
#define DISPLAY_B_DIM 200

#define CHARLIE_1 D5
#define CHARLIE_2 D6
#define CHARLIE_3 D7

#define INDICATOR_A D1
#define INDICATOR_B D2

unsigned long detectionStart;
unsigned long buttonDetect;
int buttonLast = 0;

bool connectWifi()
{
    Serial.print("Connecting");

    WiFi.persistent(false);
    WiFi.hostname(HOSTNAME);
    WiFi.mode(WIFI_STA);
    WiFi.begin(WIFISSID, WIFIPASS);

    analogWrite(INDICATOR_B, DISPLAY_B_DIM);
    unsigned long connectStart = millis();

    while (WiFi.status() != WL_CONNECTED) {
        if (WiFi.status() == WL_CONNECT_FAILED) {
            Serial.println("");
            Serial.println("Failed to connect");
            return false;
        }

        delay(100);
        Serial.print(".");

        if (millis() - connectStart > 15000) {
            Serial.println("");
            Serial.println("Timed out");
            return false;
        }
    }

    Serial.println("");
    Serial.print("IP address: ");
    Serial.println(WiFi.localIP());

    MDNS.begin(HOSTNAME);

    ArduinoOTA.setHostname(HOSTNAME);
    ArduinoOTA.begin();

    digitalWrite(INDICATOR_B, LOW);
    return true;
}

bool checkVoltage()
{
    bool ret;

#ifdef VOLTAGE_FROM_VCC
    uint16_t v = ESP.getVcc();
    ret = (v >= MIN_VOLTAGE);
#else
    float v = (analogRead(A0) / 1023.0) * 4.5;
    ret = true;
#endif

    Serial.print("Voltage: ");
    Serial.println(v);

    return ret;
}

// return 0 for no button
int readButton()
{
    pinMode(CHARLIE_2, INPUT_PULLUP);
    pinMode(CHARLIE_3, INPUT_PULLUP);
    pinMode(CHARLIE_1, OUTPUT);
    digitalWrite(CHARLIE_1, LOW);
    //delay(1);

    if (digitalRead(CHARLIE_2) == LOW) {
        return 1;
    }
    if (digitalRead(CHARLIE_3) == LOW) {
        return 5;
    }

    pinMode(CHARLIE_1, INPUT_PULLUP);
    pinMode(CHARLIE_3, INPUT_PULLUP);
    pinMode(CHARLIE_2, OUTPUT);
    digitalWrite(CHARLIE_2, LOW);
    //delay(1);

    if (digitalRead(CHARLIE_1) == LOW) {
        return 2;
    }
    if (digitalRead(CHARLIE_3) == LOW) {
        return 3;
    }

    pinMode(CHARLIE_1, INPUT_PULLUP);
    pinMode(CHARLIE_2, INPUT_PULLUP);
    pinMode(CHARLIE_3, OUTPUT);
    digitalWrite(CHARLIE_3, LOW);
    //delay(1);

    if (digitalRead(CHARLIE_1) == LOW) {
        return 6;
    }
    if (digitalRead(CHARLIE_2) == LOW) {
        return 4;
    }

    pinMode(CHARLIE_1, INPUT_PULLUP);
    pinMode(CHARLIE_2, INPUT_PULLUP);
    pinMode(CHARLIE_3, INPUT_PULLUP);

    return 0;
}

void blinkError(int count)
{
    for (int ix = 0; ix < count; ix++) {
        digitalWrite(INDICATOR_B, HIGH);
        delay(500);
        digitalWrite(INDICATOR_B, LOW);
        delay(500);
    }
}

void setup()
{
    pinMode(A0, INPUT);
    pinMode(LED_BUILTIN, OUTPUT);
    pinMode(INDICATOR_A, OUTPUT);
    pinMode(INDICATOR_B, OUTPUT);
    pinMode(CHARLIE_1, INPUT_PULLUP);
    pinMode(CHARLIE_2, INPUT_PULLUP);
    pinMode(CHARLIE_3, INPUT_PULLUP);

    digitalWrite(INDICATOR_A, LOW);
    digitalWrite(INDICATOR_B, LOW);

    Serial.begin(115200);
    //while (!Serial) { }

    Serial.println("Device started");

    if (!checkVoltage()) {
        Serial.println("Under voltage, exiting");
        blinkError(4);
        ESP.deepSleep(0);
    }

    if (!connectWifi()) {
        blinkError(6);
        ESP.deepSleep(0);
    }

    analogWrite(INDICATOR_A, DISPLAY_A_DIM);
    detectionStart = millis();
}

void loop()
{
    ArduinoOTA.handle();

    if (Update.isRunning()) {
        delay(10);
        yield();

        return;
    }

    bool goSleep = false;

    int button = readButton();

    unsigned long now = millis();
    if (button != buttonLast) {
        buttonLast = button;
        buttonDetect = now;
    }
    else if (button != 0 && now - buttonDetect > 200) {
        Serial.print("Button is: ");
        Serial.println(button);

        digitalWrite(INDICATOR_A, HIGH);

        unsigned long opStart = millis();

        if (!playMedia(button-1)) {
            blinkError(2);
        }
        else {
            // success! delay a bit so the LED has a chance to show
            unsigned long opDuration = millis() - opStart;
            if (opDuration < 1000) {
                delay(1000 - opDuration);
            }
        }

        goSleep = true;
    }

    if (goSleep || now - detectionStart > 30000) {
        Serial.println("Going into deep sleep");
        digitalWrite(INDICATOR_A, LOW);
        ESP.deepSleep(SLEEP_SECONDS * 1e6);
    }
}
