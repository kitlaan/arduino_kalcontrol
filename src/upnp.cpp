#include "config.h"
#include "upnp.h"

#include <stdio.h>
#include <Arduino.h>

#include <ESP8266HTTPClient.h>

struct {
    bool avt;
    const char* url;
    const char* md;
    bool repeat;
} media[6] = {
    MEDIA_1,
    MEDIA_2,
    MEDIA_3,
    MEDIA_4,
    MEDIA_5,
    MEDIA_6,
};


static bool sendHTTP(const char* path, const char* action, const char* payload)
{
    Serial.print("Connecting... ");
    Serial.println(action);

    WiFiClient client;
    HTTPClient http;

    char endpoint[128];
    snprintf(endpoint, sizeof(endpoint), "http://%s:1400/%s", TARGET_HOST, path);
    Serial.println(endpoint);

    http.begin(client, endpoint);
    http.addHeader("Content-Type", "text/xml; charset=\"utf-8\"");
    http.addHeader("Connection", "close");
    http.addHeader("SOAPAction", action);

    int httpCode = http.POST(payload);
    http.end();

    Serial.println(httpCode);
    return httpCode == 200;
}

bool playMedia(int selection)
{
    if (selection < 0 || selection > 5) {
        return false;
    }

    bool ret;
    char buffer[2048];

    const char* SETAVT =
        "<s:Envelope xmlns:s=\"http://schemas.xmlsoap.org/soap/envelope/\""
                " s:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">"
            "<s:Body>"
            "<u:SetAVTransportURI xmlns:u=\"urn:schemas-upnp-org:service:AVTransport:1\">"
            "<InstanceID>0</InstanceID>"
            "<CurrentURI>%s</CurrentURI>"
            "<CurrentURIMetaData>%s</CurrentURIMetaData>"
            "</u:SetAVTransportURI>"
            "</s:Body>"
        "</s:Envelope>";

    if (media[selection].avt) {
        snprintf(buffer, sizeof(buffer), SETAVT, media[selection].url, media[selection].md);
    }
    else {
        snprintf(buffer, sizeof(buffer), SETAVT, "x-rincon-queue:" TARGET_UUID "#0", "");
    }
    ret = sendHTTP("MediaRenderer/AVTransport/Control",
                   "\"urn:schemas-upnp-org:service:AVTransport:1#SetAVTransportURI\"",
                   buffer);
    if (!ret) {
        return false;
    }

    if (!media[selection].avt) {
        const char* CLEAR =
            "<s:Envelope xmlns:s=\"http://schemas.xmlsoap.org/soap/envelope/\""
                    " s:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">"
                "<s:Body>"
                "<u:RemoveAllTracks xmlns:u=\"urn:schemas-sonos-com:service:Queue:1\">"
                "<QueueID>0</QueueID>"
                "<UpdateID>0</UpdateID>"
                "</u:RemoveAllTracks>"
                "</s:Body>"
            "</s:Envelope>";

        ret = sendHTTP("MediaRenderer/Queue/Control",
                       "\"urn:schemas-sonos-com:service:Queue:1#RemoveAllTracks\"",
                       CLEAR);
        if (!ret) {
            return false;
        }

        const char* ENQUEUE =
            "<s:Envelope xmlns:s=\"http://schemas.xmlsoap.org/soap/envelope/\""
                    " s:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">"
                "<s:Body>"
                "<u:AddURIToQueue xmlns:u=\"urn:schemas-upnp-org:service:AVTransport:1\">"
                "<InstanceID>0</InstanceID>"
                "<EnqueuedURI>%s</EnqueuedURI>"
                "<EnqueuedURIMetaData>%s</EnqueuedURIMetaData>"
                "<DesiredFirstTrackNumberEnqueued>0</DesiredFirstTrackNumberEnqueued>"
                "<EnqueueAsNext>0</EnqueueAsNext>"
                "</u:AddURIToQueue>"
                "</s:Body>"
            "</s:Envelope>";

        snprintf(buffer, sizeof(buffer), ENQUEUE, media[selection].url, media[selection].md);
        ret = sendHTTP("MediaRenderer/AVTransport/Control",
                       "\"urn:schemas-upnp-org:service:AVTransport:1#AddURIToQueue\"",
                       buffer);
        if (!ret) {
            return false;
        }
    }

    const char* REPEATALL =
        "<s:Envelope xmlns:s=\"http://schemas.xmlsoap.org/soap/envelope/\""
                " s:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">"
            "<s:Body>"
            "<u:SetPlayMode xmlns:u=\"urn:schemas-upnp-org:service:AVTransport:1\">"
            "<InstanceID>0</InstanceID>"
            "<NewPlayMode>%s</NewPlayMode>"
            "</u:SetPlayMode>"
            "</s:Body>"
        "</s:Envelope>";

    snprintf(buffer, sizeof(buffer), REPEATALL, media[selection].repeat ? "REPEAT_ALL" : "NORMAL");
    ret = sendHTTP("MediaRenderer/AVTransport/Control",
                   "\"urn:schemas-upnp-org:service:AVTransport:1#SetPlayMode\"",
                   buffer);

    const char* PLAY =
        "<s:Envelope xmlns:s=\"http://schemas.xmlsoap.org/soap/envelope/\""
                " s:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">"
            "<s:Body>"
            "<u:Play xmlns:u=\"urn:schemas-upnp-org:service:AVTransport:1\">"
            "<InstanceID>0</InstanceID>"
            "<Speed>1</Speed>"
            "</u:Play>"
            "</s:Body>"
        "</s:Envelope>";

    ret = sendHTTP("MediaRenderer/AVTransport/Control",
                   "\"urn:schemas-upnp-org:service:AVTransport:1#Play\"",
                   PLAY);

    return ret;
}
