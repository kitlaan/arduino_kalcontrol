#ifndef CONFIG_H
#define CONFIG_H

// Settings defined using platformio_local.ini
#if !defined(HOSTNAME) || !defined(WIFISSID) || !defined(WIFIPASS)
  #error Missing settings from build configuration
#endif

// Settings defined using platformio_local.ini
#if !defined(TARGET_HOST) || !defined(TARGET_UUID)
  #error Missing settings from build configuration
#endif

#include "config_local.h"
#if 0
#define MEDIA_1 { \
    false, \
    "file:///jffs/settings/savedqueues.rsq#1", \
    "", \
    false }

#define MEDIA_2 { \
    false, \
    "file:///jffs/settings/savedqueues.rsq#2", \
    "", \
    false}

#define MEDIA_3 { \
    true, \
    "x-file-cifs://server/music/filename.mp3", \
    "", \
    false }

#define MEDIA_4 { \
    true, \
    "http://server/music/filename.mp3", \
    "", \
    false }

#define MEDIA_5 { \
    true, \
    "http://server/music/filename.mp3", \
    "", \
    true }

#define MEDIA_6 { \
    true, \
    "http://server/music/filename.mp3", \
    "", \
    true }
#endif

#endif // CONFIG_H
